import speech_recognition
import pyttsx3
from datetime import date
from datetime import datetime

robot_mouth = pyttsx3.init()
robot_ear = speech_recognition.Recognizer()

robot_brain = ""
while True:
    with speech_recognition.Microphone() as mic:
        print("Robot: I'm Listening")
        audio = robot_ear.listen(mic)

    print("Robot...")

    try:
        you = robot_ear.recognize_google(audio)
    except:
        you = ""

    if you == "":
        robot_brain = "I can't hear you, try again"
    elif "hello" in you:
        robot_brain = "hello Quoc Trung"
    elif "today" in you:
        today = date.today()

        robot_brain = today.strftime("%B %d, %Y")
    elif "time" in you:
        now = date.now()

        robot_brain = now.strftime("%H hours %M minutes %S second")
    elif "bye" in you:
        robot_brain = "See you next time"

        voices = robot_mouth.getProperty('voices')
        robot_mouth.setProperty('voice', voices[1].id)

        robot_mouth.say(robot_brain)
        robot_mouth.runAndWait()

        break
    else:
        robot_brain = "I'm fine thank you and you"

    print("Robot brain: " + robot_brain)

    voices = robot_mouth.getProperty('voices')
    robot_mouth.setProperty('voice', voices[1].id)

    robot_mouth.say(robot_brain)
    robot_mouth.runAndWait()
