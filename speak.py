import pyttsx3

robot_brain = "I can't hear anything"

robot_mouth = pyttsx3.init()

voices = robot_mouth.getProperty('voices')
robot_mouth.setProperty('voice', voices[1].id)

robot_mouth.say(robot_brain)
robot_mouth.runAndWait()
